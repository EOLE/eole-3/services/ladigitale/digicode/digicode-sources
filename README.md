# Digicode

Digicode est une interface graphique simple pour générer des codes QR.

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte HKGrotesk (Sil Open Font Licence 1.1) et la librairie js QR Code Styling (https://github.com/kozakdenys/qr-code-styling - MIT).

### Démo
https://ladigitale.dev/digicode/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

